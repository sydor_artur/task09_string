package com.epam;

import com.epam.controller.Controller;
import com.epam.model.BigTask;
import com.epam.model.StringUtil;
import com.epam.view.MyInternationalView;
import com.epam.view.ViewForBigTask;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;

/**
 * Start point of application.
 */
public class App {
    private static final Logger logger = LogManager.getLogger("InfoForUser");

    /**
     * This method start a program.
     */
    public static void main(String[] args) {
        //Test StringUtil
        StringUtil stringUtil = new StringUtil();
        Controller controller = new Controller(stringUtil);
        new MyInternationalView(controller).show();
        //Test BigTask
        try {
            BigTask bigTask = new BigTask();
            Controller controller1 = new Controller(bigTask);
            new ViewForBigTask(controller1);
        } catch (IOException e) {
            logger.info("File isn`t available at the moment");
        }

    }

}
