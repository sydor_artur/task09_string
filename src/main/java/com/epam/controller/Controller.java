package com.epam.controller;

import com.epam.model.BigTask;
import com.epam.model.StringUtil;

import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * Link model part and view.
 */
public class Controller {
    private StringUtil stringUtil;
    private BigTask bigTask;

    /**
     * Constructor for StringUtil.
     */
    public Controller(StringUtil stringUtil) {
        this.stringUtil = stringUtil;
    }

    /**
     * Constructor for BigTask.
     */
    public Controller(BigTask bigTask) {
        this.bigTask = bigTask;
    }

    /**
     * Concat many strings.
     */
    public String doConcat(Object... args) {
        return stringUtil.myConcat(args);
    }

    /**
     * Read information from file.
     */
    public String[] getText() {
        try {
            return bigTask.readFromFile();
        } catch (IOException e) {
            return null;
        }
    }

    /**
     * Find sentences with the biggest number
     * of same words.
     */
    public Map<String, Long> getSentenceWithGreatNumberOfSame(String[] strings) {
        return bigTask.countSentenceWithSameWord(strings);
    }

    /**
     * @return sorted text by number of words in sentence.
     */
    public String[] sortByNumberOfWords(String[] strings) {
        return bigTask.sortByNumberOfStrings(strings);
    }

    /**
     * Find unique word from first sentence.
     * And determine if it isn`t present at last of text.
     */
    public String findUniqueInText(String[] strings) {
        return bigTask.findUniqueWordFromFirstSentence(strings);
    }

    /**
     * Find words of certain length in questions.
     */
    public List<String> findWordByLength(String[] strings, Integer length) {
        return bigTask.findWordByLengthInQuestion(strings, length);
    }

    /**
     * In every sentence swap the first word that begins of vowel letter with the longest one.
     */
    public String[] swapLongestWord(String[] strings) {
        return bigTask.swapWord(strings);
    }
}
