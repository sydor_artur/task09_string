package com.epam.model;

import java.io.*;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Big task to work with text.
 */
public class BigTask {
    private File file = new File("info.txt");
    private BufferedReader bufferedReader;

    /**
     * Constructor.
     *
     * @throws IOException if file don`t exist.
     */
    public BigTask() throws IOException {
        bufferedReader = new BufferedReader(new FileReader(file));
    }

    /**
     * Read text from file.
     *
     * @return sentences from file.
     * @throws IOException if cannot read from file.
     */
    public String[] readFromFile() throws IOException {
        String[] strings = null;
        while (bufferedReader.ready()) {
            strings = bufferedReader.readLine().split("(?<=[.?!])");
        }
        for (int i = 0; i < strings.length; i++) {
            strings[i] = strings[i].trim().replaceAll("[\\t\\s]+", " ");
        }
        return strings;
    }

    /**
     * Find sentences with the biggest number
     * of same words.
     */
    public Map<String, Long> countSentenceWithSameWord(String[] strings) {
        List<Map<String, Long>> list = new ArrayList<>();
        Map<String, Long> resultMap;
        for (String string : strings) {
            Map<String, Long> tempMap = Arrays.stream(string.split("[()',. ]"))
                    .map(String::toLowerCase)
                    .filter(s -> !s.equals(""))
                    .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
            list.add(tempMap);
        }
        Long maxOfSame = findMaxNumberOfSame(list);
        resultMap = new HashMap<>();
        int counter = 0;
        for (Map<String, Long> map : list) {
            for (Long value : map.values()) {
                if (value == maxOfSame) {
                    resultMap.put(strings[counter], maxOfSame);
                }
            }
            counter++;
        }
        return resultMap;
    }

    /**
     * Find max number of same word in one sentence.
     */
    private Long findMaxNumberOfSame(List<Map<String, Long>> list) {
        Long maxValue = 0L;
        for (Map<String, Long> map : list) {
            Long tempMax;
            Optional<Map.Entry<String, Long>> maxEntry = map.entrySet()
                    .stream()
                    .max(Comparator.comparing(Map.Entry::getValue));
            tempMax = maxEntry.get().getValue();
            if (maxValue < tempMax) {
                maxValue = tempMax;
            }
        }
        return maxValue;
    }

    /**
     * @return sorted text by number of words in sentence.
     */
    public String[] sortByNumberOfStrings(String[] strings) {
        Comparator<String> comparator = (s1, s2) -> {
            return Integer.compare(s1.split("\\s+").length, s2.split("\\s+").length);
        };
        Arrays.sort(strings, comparator);
        return strings;
    }

    /**
     * Find unique word from first sentence.
     * And determine if it isn`t present at last of text.
     */
    public String findUniqueWordFromFirstSentence(String[] strings) {
        List<String> firstSentence = Arrays.stream(strings[0].split("[()',.?! ]"))
                .map(String::toLowerCase)
                .distinct()
                .collect(Collectors.toList());
        List<String> otherText = new ArrayList<>();
        for (int i = 1; i < strings.length; i++) {
            List<String> tempList = Arrays.stream(strings[i].split("[()',. ]"))
                    .map(String::toLowerCase)
                    .distinct()
                    .collect(Collectors.toList());
            otherText = Stream.concat(otherText.stream(), tempList.stream())
                    .collect(Collectors.toList());
        }
        if (!otherText.containsAll(firstSentence)) {
            String uniqueWords = "";
            for (String str : firstSentence) {
                if (!otherText.contains(str)) {
                    uniqueWords += str + ", ";
                }
            }
            return uniqueWords.length() > 0 ? uniqueWords : "No unique word.";
        } else {
            return "No unique word.";
        }
    }

    /**
     * Find words of certain length in questions.
     */
    public List<String> findWordByLengthInQuestion(String[] strings, int length) {
        List<String> resultList = new ArrayList<>();
        for (String sentence : strings) {
            if (sentence.contains("?")) {
                for (String word : sentence.split("[()',.?! ]")) {
                    if (word.length() == length) {
                        resultList.add(word);
                    }
                }
            }
        }
        return resultList;
    }

    /**
     * In every sentence swap the first word that begins of vowel letter with the longest one.
     */
    public String[] swapWord(String[] strings) {
        String[] result = new String[strings.length];
        int counter = 0;
        for (String str : strings) {
            List<String> tempList = Arrays.stream(str.split("[()',.?! ]"))
                    .filter(s -> !s.equals(""))
                    .collect(Collectors.toList());
            int indexOfVowel = 0;
            String startVowel = "";
            for (String s : tempList) {
                if (s.matches("(?i)^[aeiouy].*")) {
                    startVowel = s;
                    break;
                }
                indexOfVowel++;
            }
            String max = Collections.max(tempList, Comparator.comparing(String::length));
            int maxIndex = tempList.indexOf(max);
            if (indexOfVowel < maxIndex) {
                String temp = str.replaceFirst(max, startVowel);
                temp = temp.replaceFirst(startVowel, max);
                result[counter] = temp;
            } else if (indexOfVowel > maxIndex) {
                String temp = str.replaceFirst(startVowel, max);
                temp = temp.replaceFirst(max, startVowel);
                result[counter] = temp;
            } else {
                result[counter] = str;
            }
            counter++;
        }
        return result;
    }
}
