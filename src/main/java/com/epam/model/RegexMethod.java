package com.epam.model;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegexMethod {
    private Pattern pattern = Pattern.compile("^[A-Z].*[.]$");
    private Pattern pattern_2 = Pattern.compile("the|you", Pattern.CASE_INSENSITIVE);
    private Pattern pattern_3 = Pattern.compile("[aeiouy]", Pattern.CASE_INSENSITIVE);
    private Matcher matcher;

    public boolean BeginCapitalEndDot(String string) {
        matcher = pattern.matcher(string);
        return matcher.matches();
    }

    public String[] splitByTheOrYou(String string) {
        return string.split(pattern_2.toString());
    }

    public String replaceVowels(String string) {
        return string.replaceAll(pattern_3.toString(), "_");
    }
}
