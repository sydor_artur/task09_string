package com.epam.view;


import com.epam.controller.Controller;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;

/**
 * International view.
 */
public class MyInternationalView implements View {
    private final Logger logger = LogManager.getLogger("InfoForUser");
    private Controller controller;
    private Map<String , String> menu;
    private Map<String, Printable> menuMethod;
    private Scanner scanner = new Scanner(System.in);

    private Locale locale;
    private ResourceBundle bundle;

    private void setMenu() {
        menu = new LinkedHashMap<>();
        menu.put("1", bundle.getString("1"));
        menu.put("2", bundle.getString("2"));
        menu.put("3", bundle.getString("3"));
        menu.put("4", bundle.getString("4"));
        menu.put("5", bundle.getString("5"));
        menu.put("6", bundle.getString("6"));
    }


    /**
     *Constructor.
     */
    public MyInternationalView(Controller controller) {
        this.controller = controller;
        locale = new Locale("uk");
        bundle =  ResourceBundle.getBundle("MyMenu", locale);
        setMenu();
        menuMethod = new LinkedHashMap<>();
        menuMethod.put("1", this::pressButton1);
        menuMethod.put("2", this::UkrainianMenu);
        menuMethod.put("3", this::FranceMenu);
        menuMethod.put("4", this::PolishMenu);
        menuMethod.put("5", this::EnglishMenu);
    }

    /**
     * Test class StringUtil.
     */
    private void pressButton1() {
        String str = "Hi";
        Integer integer = 30;
        Double d = 3.14;
        Object obj = new Object();
        logger.info("Concat string, integer, double and object");
        logger.info(controller.doConcat(str, integer, d, obj));
    }

    /**
     * Set ukrainian menu.
     */
    private void UkrainianMenu() {
        locale = new Locale("uk");
        bundle = ResourceBundle.getBundle("MyMenu", locale);
        setMenu();
        show();
    }

    /**
     * Set france menu.
     */
    private void FranceMenu() {
        locale = new Locale("fr");
        bundle = ResourceBundle.getBundle("MyMenu", locale);
        setMenu();
        show();
    }

    /**
     * Set polish menu.
     */
    private void PolishMenu() {
        locale = new Locale("pl");
        bundle = ResourceBundle.getBundle("MyMenu", locale);
        setMenu();
        show();
    }

    /**
     * Set english menu.
     */
    private void EnglishMenu() {
        locale = new Locale("en");
        bundle = ResourceBundle.getBundle("MyMenu", locale);
        setMenu();
        show();
    }

    private void printMenu() {
        logger.info("MENU");
        for(String str : menu.values()) {
            logger.info(str);
        }
    }

    /**
     * Print information in console.
     */
    @Override
    public void show() {
        String key;
        do {
            printMenu();
            logger.info("Make choice: ");
            key = scanner.next();
            try {
                menuMethod.get(key).print();
            } catch (Exception e)  {
                logger.info("Bye");
            }
        } while (!key.equals("6"));
    }
}
