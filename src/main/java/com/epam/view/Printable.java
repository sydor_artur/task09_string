package com.epam.view;

/**
 * Interface Printable.
 */
public interface Printable {
    /**
     * Print information in console.
     */
    void print();
}
