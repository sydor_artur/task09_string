package com.epam.view;

/**
 * Interface for view part.
 */
public interface View {
    /**
     * show info.
     */
    void show();
}
