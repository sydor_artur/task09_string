package com.epam.view;

import com.epam.controller.Controller;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Special view for BigTask.
 */
public class ViewForBigTask {
    private final Logger logger = LogManager.getLogger("InfoForUser");
    private Controller controller;

    /**
     * Constructor.
     */
    public ViewForBigTask(Controller controller) {
        this.controller = controller;
    }

    /**
     * Show all info from bigTask.
     */
    public void show() {
        String[] strings = controller.getText();
        for (String s : strings) {
            logger.info(s);
        }

        logger.info("*****************************************************************************");
        String[] sortStrings = controller.sortByNumberOfWords(strings);
        for (String str : sortStrings) {
            logger.info(str);
        }

        logger.info("*****************************************************************************");
        controller.getSentenceWithGreatNumberOfSame(strings).forEach((k, v) ->
                logger.info("Sentence = " + k + "\nNumber of same: " + v));

        logger.info("*****************************************************************************");
        logger.info("Unique words: " + controller.findUniqueInText(strings));

        logger.info("*****************************************************************************");
        controller.findWordByLength(strings, 2).forEach
                (s -> logger.info(s + "|length = " + s.length()));

        logger.info("*****************************************************************************");
        String[] strings1 = controller.swapLongestWord(strings);
        for (String s : strings1) {
            logger.info(s);
        }
    }
}
